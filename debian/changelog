libsys-mmap-perl (0.20-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + libsys-mmap-perl: Add Multi-Arch: same.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.6.1.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Aug 2022 16:24:38 +0200

libsys-mmap-perl (0.20-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.20
  * Add myself to debian/copyright for debian/* packaging files
  * Declare compliance with Debian policy 4.5.0
  * Drop "Spelling error in manpage" patch (applied upstream)
  * Set Rules-Requires-Root to no
  * debian/watch: use uscan version 4

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 14 Feb 2020 15:32:35 +0100

libsys-mmap-perl (0.19-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.19
  * Add myself to Uploaders
  * Declare compliance with Debian policy 4.1.1
  * Wrap and sort fields in debian/control file
  * Add patch to fix spelling error in Sys::Mmap manpage
  * debian/rules: Build enabling all hardening flags

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 12 Nov 2017 15:33:03 +0100

libsys-mmap-perl (0.17-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Use debhelper 9.20120312 to get all hardening flags.
  * Add debian/upstream/metadata
  * Import upstream version 0.17
  * Update Upstream-Contact in debian/copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Oct 2014 19:37:08 +0200

libsys-mmap-perl (0.16-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Maximilian Gass ]
  * New upstream release
  * Remove useless README
  * Update copyright format

 -- Maximilian Gass <mxey@cloudconnected.org>  Sat, 20 Aug 2011 14:07:26 +0200

libsys-mmap-perl (0.15-1) unstable; urgency=low

  * Initial Release. (Closes: #626837)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Fri, 20 May 2011 15:07:31 +0200
